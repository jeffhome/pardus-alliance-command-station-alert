// ==UserScript==
// @name        Alliance Command Station Audio Alerter
// @namespace   http://userscripts.xcom-alliance.info/
// @author      Miche (Orion) / Sparkle (Artemis)
// @version     1.01
// @description Sounds an audio alert every 60 seconds when an ACS message is pending
// @include     http*://*.pardus.at/msgframe.php
// @updateURL   http://userscripts.xcom-alliance.info/alliance_command_station_alert/pardus_acs_alerter.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/alliance_command_station_alert/pardus_acs_alerter.user.js
// @icon        http://userscripts.xcom-alliance.info/alliance_command_station_alert/icon.png
// @grant       GM_getValue
// @grant       GM_setValue
// ==/UserScript==
